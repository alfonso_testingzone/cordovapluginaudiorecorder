package com.cordovaplugn.media;


import com.cordovaplugn.media.WavAudioRecorder;
import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaArgs;
import org.apache.cordova.CordovaPlugin;
import org.json.JSONArray;
import org.json.JSONException;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.media.MediaRecorder;
import android.os.Handler;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import android.media.AudioFormat;
import android.media.AudioRecord;

import java.io.FileOutputStream;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.FileNotFoundException;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.RandomAccessFile;


import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaArgs;
import org.apache.cordova.CordovaPlugin;
import org.json.JSONArray;
import org.json.JSONException;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.media.MediaRecorder;
import android.os.Handler;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import android.media.AudioFormat;
import android.media.AudioRecord;

/**
 * This class echoes a string called from JavaScript.
 */
public class AudioRecorder extends CordovaPlugin {

	private static final String LOG_TAG = "AudioRecordPlugin";

	Activity context;
	private CallbackContext callback;

	//Permission
	private String [] permissions = {Manifest.permission.RECORD_AUDIO};
	private static final int REQUEST_RECORD_AUDIO_PERMISSION = 200;
	private boolean permissionToRecordAccepted = false;
	private static String PERMISSION_DENIED_ERROR = "PERMISSION DENIED";

	//File
	private static String mFileName = null;

	//Recorder
	private WavAudioRecorder recorder = null;
    private Thread recordingThread = null;
    private boolean isRecording = false;

	//rates for recorder config
	private static final int RECORDER_SAMPLERATE = 8000;
    private static final int RECORDER_CHANNELS = AudioFormat.CHANNEL_IN_MONO;
    private static final int RECORDER_AUDIO_ENCODING = AudioFormat.ENCODING_PCM_16BIT;
    int BufferElements2Rec = 1024; // want to play 2048 (2K) since 2 bytes we use only 1024
    int BytesPerElement = 2; // 2 bytes in 16bit format

	@Override
	public void onRequestPermissionResult(int requestCode, String[] permissions, int[] grantResults) throws JSONException {
		// TODO Auto-generated method stub
		//super.onRequestPermissionResult(requestCode, permissions, grantResults);

		/*for(int r:grantResults)
		{
			if(r == PackageManager.PERMISSION_DENIED)
			{
				callback.sendPluginResult(new PluginResult(PluginResult.Status.ERROR, PERMISSION_DENIED_ERROR));
				Toast.makeText(context, PERMISSION_DENIED_ERROR, Toast.LENGTH_LONG).show();
				return;
			}
		}*/

		switch (requestCode){
		case REQUEST_RECORD_AUDIO_PERMISSION:
			permissionToRecordAccepted  = grantResults[0] == PackageManager.PERMISSION_GRANTED;
			break;
		}

		if (!permissionToRecordAccepted ){
			Toast.makeText(context, PERMISSION_DENIED_ERROR, Toast.LENGTH_LONG).show();
			callback.error(PERMISSION_DENIED_ERROR);
			return;
		}
	}


    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        callback = callbackContext;
		context = cordova.getActivity();

		cordova.requestPermissions(this, REQUEST_RECORD_AUDIO_PERMISSION, permissions);

		mFileName = context.getFilesDir().getAbsolutePath(); // getExternalCacheDir().getAbsolutePath();
		mFileName += "/audiorecordtest.pcm";

		if("startRecord".equals(action)){
			Long timeToRecord = args.optLong(0);

			startRecording();
			Handler handler = new Handler();
			Runnable runnable = new Runnable() {
				@Override
				public void run() {
					stopRecording();
				}
			};
			handler.postDelayed(runnable, timeToRecord * 1000);
			return true;
		}else if("stopRecord".equals(action)){
			stopRecording();
			return true;
		}else{
			callback.error("Incorrect action name");
			return false;
		}

    }

    private void startRecording() {
		try{
       recorder =  WavAudioRecorder.getInstanse();
	   recorder.setOutputFile(mFileName);
		recorder.prepare();
       recorder.start();

       isRecording = true;

		}catch(Exception e){
			callback.error("error start recording");
		}

    }

   private void writeAudioDataToFile() {
        // Write the output audio in byte
/*
       short sData[] = new short[BufferElements2Rec];

       FileOutputStream os = null;
        try {
            os = new FileOutputStream(mFileName);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

       while (isRecording) {
            // gets the voice output from microphone to byte format
            recorder.read(sData, 0, BufferElements2Rec);
            System.out.println("Short wirting to file" + sData.toString());
            try {
                // writes the data to file from buffer stores the voice buffer
                byte bData[] = short2byte(sData);

               os.write(bData, 0, BufferElements2Rec * BytesPerElement);

           } catch (IOException e) {
                e.printStackTrace();
            }
        }

       try {
            os.close();
        } catch (IOException e) {
            e.printStackTrace();
        }*/
    }

   private byte[] short2byte(short[] sData) {
        int shortArrsize = sData.length;
        byte[] bytes = new byte[shortArrsize * 2];

       for (int i = 0; i < shortArrsize; i++) {
            bytes[i * 2] = (byte) (sData[i] & 0x00FF);
            bytes[(i * 2) + 1] = (byte) (sData[i] >> 8);
            sData[i] = 0;
        }
        return bytes;
    }

   private void stopRecording() {
	   try{
        if (null != recorder) {
            isRecording = false;


           recorder.stop();
            recorder.release();

           recorder = null;
            recordingThread = null;
			try{

            encodeBase64();
			}catch(Exception e){
				callback.error("error encode");
			}
        }
	   }
	   catch(Exception e){
		   callback.error("error stop recording");
	   }
        // stops the recording activity
    }

	private void encodeBase64(){
		byte[] audioBytes;
		FileInputStream fileInputStream = null;
		try{
			File audioFile = new File(mFileName);
			if(audioFile.exists()){
				fileInputStream = new FileInputStream(audioFile);
				
				audioBytes = new byte[(int) audioFile.length()];
				fileInputStream.read(audioBytes); //read file into bytes[]
            	fileInputStream.close();
				
                StringBuilder sb = new StringBuilder();
                    for (int i = 0; i < audioBytes.length; i++) {
                        sb.append( String.valueOf(audioBytes[i]));
                        if(i==(audioBytes.length-1))
                            continue;
                        else
                            sb.append(",");
                        }
						JSONArray mJSONArray = new JSONArray();
							for(int value : audioBytes)
							{
								mJSONArray.put(value);
							}
                    callback.success(mJSONArray);
                }        	
			
		}catch (Exception e){
			Log.e(LOG_TAG, "Base64 error " + e.getMessage());
			callback.error("error obteniendo bytes////"+e.getMessage());
		}finally {
			if (fileInputStream != null) {
				try {
					fileInputStream.close();
				} catch (IOException e) {
					Log.e(LOG_TAG, "Base64 IS close error: " + e.getMessage());
				}
			}
			callback.error("Base64 error");
		}
	}
}